<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "itypo_404handler".
 *
 * Auto generated 31-01-2013 13:42
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'iTypo 404/403 Handler',
	'description' => 'Proper 404 & 403 handling. URL does not change & correct HTTP status codes. (Even when logged in!) Tested with TYPO3 4.5.x - 4.7.x. Compatible with RealURL.',
	'category' => 'fe',
	'author' => 'Sander Leeuwesteijn | iTypo',
	'author_email' => 'info@itypo.nl',
	'shy' => '',
	'dependencies' => '',
	'conflicts' => 'pagenotfoundhandling,ws_404,hype_error',
	'priority' => '',
	'module' => '',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author_company' => 'iTypo',
	'version' => '1.0.0',
	'constraints' => array(
		'depends' => array(
			'typo3' => '4.5.0-4.7.99',
		),
		'conflicts' => array(
			'pagenotfoundhandling' => '',
			'ws_404' => '',
			'hype_error' => '',
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:8:{s:26:"class.itypo_404handler.php";s:4:"e48b";s:34:"class.tx_itypo404handler_hooks.php";s:4:"7b2f";s:12:"ext_icon.gif";s:4:"f028";s:17:"ext_localconf.php";s:4:"50ca";s:14:"ext_tables.php";s:4:"6769";s:14:"doc/manual.sxw";s:4:"db8e";s:37:"static/itypo_404handler/constants.txt";s:4:"b11d";s:33:"static/itypo_404handler/setup.txt";s:4:"3a56";}',
	'suggests' => array(
	),
);

?>